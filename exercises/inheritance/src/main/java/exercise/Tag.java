package exercise;

import java.util.Map;

// BEGIN
abstract class Tag {
    private String tagName;
    private Map<String, String> attributes;

    Tag(String name, Map<String, String> attr) {
        this.tagName = name;
        this.attributes = attr;
    }
    public String toString() {
        StringBuilder tag = new StringBuilder();
        tag.append("<");
        tag.append(tagName);
        for (Map.Entry<String, String> item : attributes.entrySet()) {
            tag.append(" ");
            tag.append(item.getKey());
            tag.append("=\"");
            tag.append(item.getValue());
            tag.append("\"");
        }
        tag.append(">");
        return tag.toString();
    }

    public String getTagName() {
        return tagName;
    }
}
// END
