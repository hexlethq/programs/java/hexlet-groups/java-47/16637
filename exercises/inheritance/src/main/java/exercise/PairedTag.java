package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// BEGIN
class PairedTag extends Tag {
    private String text;
    private List<Tag> children;

    PairedTag(String tagName, Map<String, String> attr, String t, List<Tag> child) {
        super(tagName, attr);
        this.text = t;
        this.children = new ArrayList<>(child);
    }
    @Override
    public String toString() {
        StringBuilder tag = new StringBuilder();
        tag.append(super.toString());
        tag.append(text);
        for (Tag item : children) {
            tag.append(item.toString());
        }
        tag.append("</");
        tag.append(getTagName());
        tag.append(">");

        return tag.toString();
    }
}
// END
