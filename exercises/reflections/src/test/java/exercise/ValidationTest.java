package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.*;


class ValidationTest {

    @Test
    void testValidate() {
        Address address1 = new Address("Russia", "Ufa", "Lenina", "54", null);
        List<String> result1 = Validator.validate(address1);
        List<String> expected1 = List.of();
        assertThat(result1).isEqualTo(expected1);

        Address address2 = new Address(null, "London", "1-st street", "5", "1");
        List<String> result2 = Validator.validate(address2);
        List<String> expected2 = List.of("country");
        assertThat(result2).isEqualTo(expected2);

        Address address3 = new Address("USA", null, null, null, "1");
        List<String> result3 = Validator.validate(address3);
        List<String> expected3 = List.of("city", "street", "houseNumber");
        assertThat(result3).isEqualTo(expected3);
    }

    // BEGIN
    @Test
    void testAdvancedValidate() {
        Address address1 = new Address("Usa", "Texas", null, "7", "2");
        Map<String, List<String>> notValidFields = Validator.advancedValidate(address1);
        Map<String, List<String>> testResult = new HashMap<>();
        List<String> errorMasg = Arrays.asList("length less than 4");
        List<String> errorMasg11 = Arrays.asList("can not be null");
        testResult.put("country", errorMasg);
        testResult.put("street", errorMasg11);
        assertThat(testResult).isEqualTo(notValidFields);

        Address address2 = new Address("Russia", null, null, null, "2");
        Map<String, List<String>> notValidFields2 = Validator.advancedValidate(address2);
        Map<String, List<String>> testResult2 = new HashMap<>();
        List<String> errorMasg2 = Arrays.asList("can not be null");
        testResult2.put("city", errorMasg2);
        testResult2.put("street", errorMasg2);
        testResult2.put("houseNumber", errorMasg2);
        assertThat(testResult2).isEqualTo(notValidFields2);

        Address address3 = new Address("Usa", "Texas", "Lenina", "54", "2");
        Map<String, List<String>> notValidFields3 = Validator.advancedValidate(address3);
        Map<String, List<String>> testResult3 = new HashMap<>();
        List<String> errorMasg3 = Arrays.asList("length less than 4");
        testResult3.put("country", errorMasg3);
        assertThat(testResult3).isEqualTo(notValidFields3);

    }
    // END
}
