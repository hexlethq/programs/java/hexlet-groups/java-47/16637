package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static final String LESST_HAN = "length less than 4";
    public static final String CANT_BE_NULL = "can not be null";

    public static List<String> validate(Address address) {
        List<String> nullField = new ArrayList<>();
        Field[] fields = address.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getAnnotation(NotNull.class) != null) {
                try {
                    if (field.get(address) == null) {
                        nullField.add(field.getName());
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return nullField;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {

        Map<String, List<String>> result = new HashMap<>();

        Field[] fields = address.getClass().getDeclaredFields();
        for (Field field : fields) {
            List<String> errorMas = new ArrayList<>();
            field.setAccessible(true);

            if (field.getAnnotation(MinLength.class) != null) {
                try {
                    MinLength min = field.getAnnotation(MinLength.class);
                    String len = (String) field.get(address);
                    if (len.length() < min.minLength()) {
                        errorMas.add(LESST_HAN);
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            if (field.getAnnotation(NotNull.class) != null) {
                try {
                    if (field.get(address) == null) {
                        errorMas.add(CANT_BE_NULL);
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            if (errorMas.size() != 0) {
                result.put(field.getName(), errorMas);
            }
        }
        return result;
    }
}
// END
