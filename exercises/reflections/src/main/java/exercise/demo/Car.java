package exercise.demo;

public class Car {
    private String name;
    private String maxSpeed;

    public String getName() {
        return name;
    }

    public void setName(String n) {
        this.name = n;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(String mSpeed) {
        this.maxSpeed = mSpeed;
    }

    @Override
    public String toString() {
        return "Car{"
                + "name='" + name + '\''
                + ", maxSpeed=" + maxSpeed
                + '}';
    }
}
