// BEGIN
package exercise.geometry;

public class Segment {


    public static double[][] makeSegment(double[] p1, double[] p2) {
        double[][] otrezok = new double[2][2];
        otrezok[0] = p1;
        otrezok[1] = p2;
        return otrezok;
    }
    
    public static double[] getBeginPoint(double[][] otrezok) {
        return otrezok[0];    
    }
    
    public static double[] getEndPoint(double[][] otrezok) {
        return otrezok[1];
    }
}
// END
