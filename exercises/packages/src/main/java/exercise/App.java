// BEGIN
package exercise;


import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] otr) {
        double[] beginPoint = Segment.getBeginPoint(otr);
        double[] endPoint = Segment.getEndPoint(otr);
        double midX = (Point.getX(beginPoint) + Point.getX(endPoint)) / 2;
        double midY = (Point.getY(beginPoint) + Point.getY(endPoint)) / 2;
        double[] midPoint = Point.makePoint(midX, midY);
        return midPoint;
    }

    public static double[][] reverse(double[][] otr) {
        double[] beginPoint = Segment.getBeginPoint(otr);
        double[] endPoint = Segment.getEndPoint(otr);
        double[] revBeginPoint = Point.makePoint(Point.getX(endPoint), Point.getY(endPoint));
        double[] revEndPoint = Point.makePoint(Point.getX(beginPoint), Point.getY(beginPoint));
        double[][] revOtr = Segment.makeSegment(revBeginPoint, revEndPoint);

        return revOtr;
    }

    public static boolean isBelongToOneQuadrant(double[][] otr) {
        double[] beginPoint = Segment.getBeginPoint(otr);
        double[] endPoint = Segment.getEndPoint(otr);
        if (((Point.getX(beginPoint) > 0) && (Point.getX(endPoint) > 0)) || ((Point.getX(beginPoint) < 0) && (Point.getX(endPoint) < 0))) {
            if (((Point.getY(beginPoint) > 0) && (Point.getY(endPoint) > 0)) || ((Point.getY(beginPoint) < 0) && (Point.getY(endPoint) < 0))) {
                return true;
            }
        }
        return false;
    }
}
// END
