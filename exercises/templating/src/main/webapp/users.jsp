<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<!-- BEGIN -->
<!DOCTYPE html>
<html>
    <head>
        <title>Users</title>
    </head>
    <body>
    	    <%
               List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");
                %>
                <table>
                <%
                        for(Map<String, String> user : users) {%>
                        <tr>
                            <td>
                           <a href="/users/show/?id=<%= user.get("id") %>"><%= user.get("firstName") %>
                           &nbsp&nbsp <%= user.get("lastName") %>
                           </a>
                       </td>
                        </tr>
                        <%}%>
                </table>
        </body>
</html>
<!-- END -->
