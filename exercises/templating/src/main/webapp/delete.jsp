<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->

<!-- END -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Map"%>
<!-- BEGIN -->
<!DOCTYPE html>
<html>
    <head>
        <title>User</title>
    </head>
    <body>
    	    <%
               Map<String, String> user = (Map<String, String>) request.getAttribute("user");
                %>
                <table>
                        <tr>
                            <H3>Вы действительно хотите удалить </H3>
                            <td><H2> &nbsp&nbsp <%= user.get("firstName") %>&nbsp<%= user.get("lastName") %>?</H2></td>
                        </tr>
                        <tr>
                            <td>
                                <form action="/users/delete?id=<%= user.get("id") %>" method="post">
                                    <button type="submit" class="btn btn-danger">Удалить</button>
                                </form>
                            </td>
                        </tr>
                </table>
        </body>
</html>
<!-- END -->