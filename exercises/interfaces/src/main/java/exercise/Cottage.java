package exercise;

// BEGIN
class Cottage implements Home {
    private double area;
    private int floor;

    public Cottage(double area, int floor) {
        this.area = area;
        this.floor = floor;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Home another) {

        if (this.getArea() > another.getArea()) {
            return 1;
        }
        if (this.getArea() < another.getArea()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return this.floor + " этажный коттедж площадью " + this.getArea() + " метров";
    }
}
// END
