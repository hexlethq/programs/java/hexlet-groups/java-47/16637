package exercise;

import java.util.*;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int n) {
        List<String> result = new ArrayList<>();
        appartments.sort((appart1, appart2) -> appart1.compareTo(appart2));

        for (int i = 0; i < n && i < appartments.size(); i++) {
            result.add(appartments.get(i).toString());
        }

        return result;
    }
}
// END
