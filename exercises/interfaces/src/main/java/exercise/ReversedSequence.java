package exercise;

// BEGIN
class ReversedSequence implements CharSequence {
    String reverseString = "";

    public ReversedSequence(String str) {
        StringBuilder revers = new StringBuilder(str);
        reverseString = revers.reverse().toString();
    }

    @Override
    public int length() {
        return reverseString.length();
    }

    @Override
    public char charAt(int i) {
        System.out.println(reverseString);
        return reverseString.charAt(i - 1);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return reverseString.substring(i, i1);
    }

    public String toString() {
        return reverseString;
    }
}
// END
