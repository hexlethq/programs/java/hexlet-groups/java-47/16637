package exercise;

// BEGIN
interface Home {
    public double getArea();
    public  int compareTo(Home another);
    public String toString();
}
// END
