package exercise;

import java.nio.file.Path;
import java.nio.file.Files;

// BEGIN
class App {

    public static void save(Path path, Car car) throws Exception {
        String file = car.serialize();
        Files.writeString(path, file);
    }

    public static Car extract(Path path) throws Exception {
        String file = Files.readString(path);
        return Car.unserialize(file);
    }
}
// END
