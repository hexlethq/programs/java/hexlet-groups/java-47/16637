package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String abr ="";
        int len = phrase.length();
        char symbol;

        for(int i = 0; i < len; i++) {
            symbol = phrase.charAt(i);
            if(symbol != ' ') {
                if(i == 0) abr = abr + Character.toUpperCase(symbol);
                else {
                    if(phrase.charAt(i - 1) == ' ') abr = abr + Character.toUpperCase(symbol);
                }
            }
        }
        return abr;
    }
    // END
}
