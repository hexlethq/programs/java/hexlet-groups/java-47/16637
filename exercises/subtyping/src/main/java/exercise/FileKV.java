package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV implements KeyValueStorage {
    private String path;

    public FileKV(String path, Map<String, String> store) {
        this.path = path;
        writeFile(store);
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> store = readFile();
        store.put(key, value);
        writeFile(store);
    }

    @Override
    public void unset(String key) {
        Map<String, String> store = readFile();
        store.remove(key);
        writeFile(store);
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> store = readFile();
        return store.getOrDefault(key,defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(readFile());
    }

    private Map<String, String> readFile() {
        String json = Utils.readFile(path);
        return Utils.unserialize(json);
    }

    private void writeFile(Map<String, String> store) {
        String json = Utils.serialize(store);
        Utils.writeFile(path, json);
    }
}
// END
