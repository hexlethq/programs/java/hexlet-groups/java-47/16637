package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
	public static String[][] enlargeArrayImage(final String[][] image) {

		if (image.length == 0) {
			return image;
		}

		String[][] doubleRowImage = Arrays.stream(image)
				.flatMap(y -> Stream.of(y, y))
				.toArray(String[][]::new);

		String[][] bigImage = Arrays.stream(doubleRowImage)
				.map(x -> Stream.of(x)
						.map(y -> y + ":" + y)
						.map(a -> a.split(":"))
						.flatMap(z -> Arrays.stream(z))
						.toArray(String[]::new))
				.toArray(String[][]::new);

		return bigImage;
	}
}
// END
