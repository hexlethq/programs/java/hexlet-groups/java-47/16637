package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        char c = sentence.charAt(sentence.length()-1);

        if(c == '!') System.out.println(sentence.toUpperCase());
        else System.out.println(sentence.toLowerCase());
        // END
    }
}
