package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        for(int i = 0; i < starsCount; i++)
            System.out.print('*');

        System.out.print(cardNumber.substring(12));

        // END
    }
}
