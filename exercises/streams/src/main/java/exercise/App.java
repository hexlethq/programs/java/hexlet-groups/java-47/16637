package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
class App {
	static List<String> freeDomenEmails = List.of("@gmail.com", "@yandex.ru","@hotmail.com");

	public static long getCountOfFreeEmails(List<String> emailsList) {
		final int count = 0;

		if(emailsList.isEmpty()) {
			return count;
		}

		return emailsList.stream()
				.filter(x -> freeDomenEmails.stream().anyMatch(y -> x.contains(y)))
			.count();
	}
}
// END
