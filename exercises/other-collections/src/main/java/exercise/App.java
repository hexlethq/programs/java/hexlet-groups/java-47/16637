package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
class App {
	public static Map<String, String> genDiff(final Map<String, Object> data1, final Map<String, Object> data2) {
		Map<String, String> diffData = new LinkedHashMap<>();
		Set<String> keysData1 = data1.keySet();
		Set<String> keysData2 = data2.keySet();
		TreeSet<String> treeData = new TreeSet<>(keysData1);
		treeData.addAll(keysData2);

		for (String key : treeData) {
			String valueData = "";

			if (!keysData1.contains(key)) {
				valueData = "added";
			} else if (!keysData2.contains(key)) {
				valueData = "deleted";
			} else if (data1.get(key).equals(data2.get(key))) {
				valueData = "unchanged";
			} else {
				valueData = "changed";
			}

			diffData.put(key, valueData);
		}

		return diffData;
	}
}
//END
