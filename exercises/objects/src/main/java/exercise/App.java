package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] text) {
        StringBuilder htmlText = new StringBuilder();
        if(text.length != 0) {
            htmlText.append("<ul>\n");
            for (int i = 0; i < text.length; i++) {
                htmlText.append("  <li>");
                htmlText.append(text[i]);
                htmlText.append("</li>\n");
            }
            htmlText.append("</ul>");
        }
        else return "";

        return htmlText.toString();
    }


    public static String getUsersByYear(String[][] text, int year){

        int len = text.length;
        if (len > 0) {
            String[] mas = new String[len];
            int j = 0;
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                for (int i = 0; i < len; i++) {
                    LocalDate date = LocalDate.parse(text[i][1], format);
                    if (year == date.getYear()) {
                        mas[j] = text[i][0];
                        j++;
                    }
                }
                String[] names = new String[j];
                for (int i = 0; i < j; i++) {
                    names[i] = mas[i];
                }
                return buildList(names);
        }
        return "";
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        int index = -1;
        DateTimeFormatter format1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        LocalDate minDate = LocalDate.parse(date, format);
        if(users.length != 0) {
            for (int i = 0; i < users.length; i++) {
                LocalDate userDate = LocalDate.parse(users[i][1], format1);
                if (minDate.isAfter(userDate)) {
                    if(index == -1) index = i;
                    else {
                        LocalDate userDate2 = LocalDate.parse(users[index][1], format1);
                        if(userDate2.isBefore(userDate)) index = i;
                    }
                }
            }

        }
        else return "";

        if(index != -1) return users[index][0];
        else return "";
        // END
    }
}
