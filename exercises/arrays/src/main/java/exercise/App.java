package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] mas) {
        int k;
        int n = mas.length;

        for(int i = 0; i < n / 2 ; i++) {
              k = mas[i];
              mas[i] = mas[n - i - 1];
              mas[n - i - 1] = k;
        }
        return mas;
    }

    public static int mult(int[] mas) {
        int sum = 1;
        for(int i:mas) {
            sum = sum * i;
        }
        return sum;
    }

    public static int[] flattenMatrix(int[][] matr) {
        int[] mas;
        int n = 0;
        if(matr.length != 0) n = matr.length * matr[0].length;
        mas = new int[n];
        int k;

        for(int i = 0; i < matr.length; i++) {
            k = matr[i].length;
            for(int j = 0; j < k; j++) {
                mas[i * k + j] = matr[i][j];
            }
        }

       return mas;
    }
    // END
}
