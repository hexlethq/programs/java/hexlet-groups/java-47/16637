### Hexlet tests and linter status:
[![Actions Status](https://github.com/evgenpush/java-project-lvl3/workflows/hexlet-check/badge.svg)](https://github.com/evgenpush/java-project-lvl3/actions)

[![Github-actions](https://github.com/evgenpush/java-project-lvl3/actions/workflows/github-actions.yml/badge.svg)](https://github.com/evgenpush/java-project-lvl3/actions/workflows/github-actions.yml)

<a href="https://codeclimate.com/github/evgenpush/java-project-lvl3"><img src="https://api.codeclimate.com/v1/badges/a99a88d28ad37a79dbf6/maintainability" /></a>
