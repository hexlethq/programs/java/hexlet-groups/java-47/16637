package exercise;

class Point {


    // BEGIN
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point makePoint(int x, int y) {
        Point point = new Point(x, y);
        return point;
    }

    public static int getX(Point point) {
        return point.x;
    }

    public static int getY(Point point) {
        return point.y;
    }

    public static String pointToString(Point point) {
        return "(" + getX(point) + ", " + getY(point) + ")";
    }

    public static int getQuadrant(Point point) {
        if((point.x == 0) || (point.y == 0)) return 0;
        else {
            if(point.x > 0) {
                if(point.y > 0) return 1;
                else return 4;
            }
            else {
                if(point.y > 0) return 2;
                else return 3;
            }
        }
    }

    public static Point getSymmetricalPointByX(Point point) {
        point.y = point.y * (-1);

        return point;
    }

    public static double calculateDistance(Point p1, Point p2) {

        double lx = Math.abs(p1.x - p2.x);
        double ly = Math.abs(p1.y - p2.y);

        return Math.sqrt((lx * lx) + (ly * ly));
    }
    // END
}
