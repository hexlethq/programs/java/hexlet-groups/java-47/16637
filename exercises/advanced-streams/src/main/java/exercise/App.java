package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
class App {
	public static String getForwardedVariables(final String content) {
		String prefix = "environment";
		String prefixEnvVariables = "X_FORWARDED_";
		String[] masLinesContent = content.split("\n");

		return Arrays.stream(masLinesContent)
				.filter(a -> a.contains(prefix))
				.map(x -> x.substring(prefix.length() + 2, x.length() - 1))
				.map(y -> y.split(","))
				.flatMap(z -> Arrays.stream(z))
				.filter(b -> b.contains(prefixEnvVariables))
				.map(c -> c.replace(prefixEnvVariables, ""))
				.collect(Collectors.joining(","));

	}
}
//END
