package exercise;

// BEGIN
class Circle {
    private Point centr;
    private int radius;

    Circle(Point point, int r) {
        this.centr = point;
        this.radius = r;
    }

    public int getRadius() {
        return radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (radius < 0) {
            throw new NegativeRadiusException("Не удалось посчитать площадь");
        }
        return Math.PI * Math.pow(radius, 2);
    }
}
// END
