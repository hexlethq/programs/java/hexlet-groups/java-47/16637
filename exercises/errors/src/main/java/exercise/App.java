package exercise;

// BEGIN
class App {
    public static void printSquare(Circle cirle) {
        try {
            int square = (int) Math.round(cirle.getSquare());
            System.out.println(String.valueOf(square));
        } catch (NegativeRadiusException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Вычисление окончено");
        }
    }
}
// END
