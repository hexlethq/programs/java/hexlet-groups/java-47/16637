package exercise.controllers;

import io.ebeaninternal.server.util.Str;
import io.javalin.http.Handler;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import io.javalin.core.validation.Validator;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.JavalinValidation;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

import exercise.domain.User;
import exercise.domain.query.QUser;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
            .orderBy()
                .id.asc()
            .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
            .id.equalTo(id)
            .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        Validator<String> firstNameValidator = ctx.formParamAsClass("firstName", String.class)
            .check(value -> !value.isEmpty(), "firstName is empty");

        Validator<String> lastNameValidator = ctx.formParamAsClass("lastName", String.class)
                .check(value -> !value.isEmpty(), "lastName is empty");

        Validator<String> emailValidator = ctx.formParamAsClass("email", String.class);
        EmailValidator emailV = EmailValidator.getInstance();
        emailValidator.check(emailV::isValid, "email not valid");

        Validator<String> passwordValidator= ctx.formParamAsClass("password", String.class)
                .check(value -> value.length() >= 4, "password less 4 letter")
                .check(value -> StringUtils.isNumeric(value), "password must have only digital");

        Map<String, List<ValidationError<? extends Object>>> errors = JavalinValidation.collectErrors(
                firstNameValidator, lastNameValidator, emailValidator, passwordValidator);

        User newUser = new User(firstNameValidator.getStringValue(), lastNameValidator.getStringValue(),
                emailValidator.getStringValue(), passwordValidator.getStringValue());

        if (!errors.isEmpty()) {
            ctx.status(422);
            ctx.attribute("errors", errors);
            ctx.attribute("user", newUser);
            ctx.render("users/new.html");
            return;
        }

        newUser.save();
        ctx.attribute("flash", "user have created");
        ctx.redirect("/users");
        // END
    };
}
