package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println((8 / 2) + (100 % 3));
        // END
    }

    public static void strings() {
        // BEGIN
        String language = "Java";

        System.out.println(language + " works on JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
