package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] mas) {
        int max;
        int k;
        int len = mas.length;

        for(int i = 0; i< len - 1; i++) {
            max = mas[0];
            k = 0;
            for(int j = 1; j < len - i; j++) {
                if(max < mas[j]) {
                    max = mas[j];
                    k = j;
                }
            }

            if(k != len - i - 1) {
                mas[k] = mas[len - i - 1];
                mas[mas.length - i - 1] = max;
            }
        }

        return mas;
    }
    // END
}
