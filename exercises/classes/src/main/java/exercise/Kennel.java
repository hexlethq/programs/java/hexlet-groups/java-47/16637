package exercise;

import java.util.LinkedList;



// BEGIN
public class Kennel {
    static String[][] puppy;
    static int len;

    public static void addPuppy(String[] p) {
        len = len + 1;
        String[][] newPuppy = new String[len][2];
        for (int i = 0; i < len - 1; i++) {
            newPuppy[i][0] = puppy[i][0];
            newPuppy[i][1] = puppy[i][1];
        }
        newPuppy[len - 1][0] = p[0];
        newPuppy[len - 1][1] = p[1];
        puppy = newPuppy;
    }

    public static void addSomePuppies(String[][] puppies) {
        int count = len;
        len = len + puppies.length;
        String[][] newPuppy = new String[len][2];
        for (int i = 0; i < count; i++) {
            newPuppy[i][0] = puppy[i][0];
            newPuppy[i][1] = puppy[i][1];
        }
        for (int i = 0; i < puppies.length; i++) {
            newPuppy[count + i][0] = puppies[i][0];
            newPuppy[count + i][1] = puppies[i][1];
        }
        puppy = newPuppy;
    }

    public static int getPuppyCount() {
        if (puppy == null) {
            return 0;
        } else {
            return puppy.length;
        }
    }

    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < len; i++) {
            if(puppy[i][0] == name) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] p = new String[len][2];
        for (int i = 0; i < len; i++) {
            p[i][0] = puppy[i][0];
            p[i][1] = puppy[i][1];
        }
        return p;
    }

    public static String[] getNamesByBreed(String breed) {
        if(len != 0) {
            int k = 0;
            String[] masBreed = new String[len];
            for (int i = 0; i < len; i++) {
                if (puppy[i][1] == breed) {
                    masBreed[k] = puppy[i][0];
                    k++;
                }
            }
            String[] breeds = new String[k];
            for(int i = 0; i < k; i++) {
                breeds[i] = masBreed[i];
            }
            return breeds;
        }
        else {
            return null;
        }
    }

    public static void resetKennel() {
        len = 0;
        puppy = null;
    }

    public static boolean removePuppy(String name) {
        boolean flag = false;
        int j = 0;
        String[][] newPuppy = new String[len][2];
        for(int i = 0; i < len; i++) {
            if (puppy[i][0] != name) {
                newPuppy[j][0] = puppy[i][0];
                newPuppy[j][1] = puppy[i][1];
                flag = true;
                j++;
            }
        }
            if (flag) {
                len = j;
                String[][] nPuppy = new String[len][2];
                for (int k = 0; k < len; k++) {
                    nPuppy[k][0] = newPuppy[k][0];
                    nPuppy[k][1] = newPuppy[k][1];
                }
                puppy = nPuppy;
            }
        return flag;
    }
}
// END
