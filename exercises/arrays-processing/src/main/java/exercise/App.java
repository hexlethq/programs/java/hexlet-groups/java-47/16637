package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] numbars) {
        int index = -1;
        int maxNegative = Integer.MIN_VALUE;
        for(int i = 0; i < numbars.length; i++) {
            if((numbars[i] < 0) && (numbars[i] > maxNegative)) {
                maxNegative = numbars[i];
                index = i;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] numbers) {
        double average;
        double summ = 0.0;
        int index = 0;
        if(numbers.length != 0) {
            for (int i : numbers) {
                summ += i;
            }
            average = summ / numbers.length;
            for (int i : numbers) {
                if (i < average) index++;
            }
            int[] mas = new int[index];
            index = 0;

            for (int i : numbers) {
                if (i < average) {
                    mas[index] = i;
                    index++;
                }
            }
            return mas;
        }
        return numbers;
    }

    public static int getSumBeforeMinAndMax(int[] mas) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int indexMax = -1;
        int indexMin = -1;
        int k,l,sum = 0;

        for(int i = 0; i < mas.length; i++) {
            if(mas[i] < min) {
                min = mas[i];
                indexMin = i;
            }
            if(mas[i] > max) {
                max = mas[i];
                indexMax = i;
            }
        }

        if(indexMax != -1 && indexMin != -1) {
           if(indexMax > indexMin) {
               k = indexMin;
               l = indexMax;
           }
           else {
               k = indexMax;
               l = indexMin;
           }

            for(int i = k + 1; i < l; i++) {
               sum += mas[i];
            }
        }

        return sum;
    }
    // END
}
