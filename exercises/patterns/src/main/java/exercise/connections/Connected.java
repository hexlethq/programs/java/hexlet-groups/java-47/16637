package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {
    private TcpConnection connect;

    public Connected(TcpConnection conect) {
        this.connect = conect;
    }
    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error! Connection already connected");
    }

    @Override
    public void disconnect() {
        connect.setState(new Disconnected(connect));
    }

    @Override
    public void write(String text) {
        System.out.println(text);
    }
}
// END
