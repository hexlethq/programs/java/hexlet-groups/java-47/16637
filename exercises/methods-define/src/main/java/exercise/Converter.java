package exercise;

class Converter {
    // BEGIN
   public static int convert(int a, String s) {

       if (s == "b") {
           return a * 1024;
       } else if (s == "Kb") {
           return a / 1024;
       } else {
           return 0;
       }
   }

   public static void main(String[] args) {
       int a = 10;
       System.out.println(a + " Kb = " + convert(a, "b") + " b");
   }
    // END
}
