package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int gradus) {
        double sSqr;
        sSqr = (a * b * Math.sin(gradus * Math.PI / 180)) / 2;

        return sSqr;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
