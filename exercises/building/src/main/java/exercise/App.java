// BEGIN
package exercise;
import com.google.gson.Gson;
class App {

    public static void main(String[] args) {
        System.out.print("Hello, World!");
    }

    public static String toJson(String[] mas) {

        Gson gson = new Gson();
        String json = gson.toJson(mas);
        return  json;
    }

}
// END
