package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

// BEGIN
public class App {
	public static List<Map<String, String>> findWhere(final List<Map<String, String>> books, final Map<String, String> bookSearchData) {
		List<Map<String, String>> resultBooks = new ArrayList<>();

		for (Map<String, String> book : books) {
			boolean isMatch = true;
			for (Map.Entry<String, String> entry : bookSearchData.entrySet()) {
				String value = book.get(entry.getKey());
				if (!entry.getValue().matches(value)) {
					isMatch = false;
					break;
				}
			}
			if (isMatch) {
				resultBooks.add(book);
			}
		}
		return resultBooks;
	}
}
//END
