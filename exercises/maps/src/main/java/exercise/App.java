package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
	public static Map<String, Integer> getWordCount(final String sentence) {
		final int defAmount = 0;
		Map<String, Integer> amountWords = new HashMap<>();

		if (sentence.length() == 0) {
			return amountWords;
		}

		String[] words = sentence.split(" ");
		for (int i = 0; i < words.length; i++) {
			Integer amount = amountWords.getOrDefault(words[i], defAmount);
			amountWords.put(words[i], ++amount);
		}

		return amountWords;
	}

	public static String toString(final Map<String, Integer> amountWords) {
		String sentence = "{}";
		if (amountWords.isEmpty()) {
			return sentence;
		}

		sentence = "{\n";
		for (Map.Entry<String, Integer> word: amountWords.entrySet()) {
			sentence = sentence + "  " + word.getKey() + ": " + word.getValue() + "\n";
		}
		sentence = sentence + "}";

		return sentence;
	}
}
//END
