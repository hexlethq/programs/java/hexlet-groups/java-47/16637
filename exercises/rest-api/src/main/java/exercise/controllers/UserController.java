package exercise.controllers;

import io.javalin.core.validation.BodyValidator;
import io.javalin.core.validation.JavalinValidation;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.Validator;
import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        ctx.json(users);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
                .id.equalTo(Integer.valueOf(id))
                .findOne();
        ctx.json(user);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        Map<String, List<ValidationError<User>>> errors = ctx.bodyValidator(User.class)
                .check(u -> !u.getFirstName().isEmpty(), "firstName is empty")
                .check(u -> !u.getLastName().isEmpty(), "lastName is empty")
                .check(u -> u.getPassword().length() >= 4 , "password less 4 letter")
                .check(u -> StringUtils.isNumeric(u.getPassword()), "password must have only numbers")
                .errors();

        if (errors.isEmpty()) {
            User user = DB.json().toBean(User.class, ctx.body());
            user.save();
            return;
        }
        ctx.status(422);
        ctx.json(errors);
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        User user = DB.json().toBean(User.class, ctx.body());
        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        User user = new QUser()
                .id.equalTo(Integer.valueOf(id))
                .findOne();
        user.delete();
        // END
    };
}
