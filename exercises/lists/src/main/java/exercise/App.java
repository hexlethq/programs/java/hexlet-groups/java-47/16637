package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(final String lettersGame, final String word) {
        List<String> letters = new ArrayList<>(Arrays.asList(lettersGame.split("")));
        String lowerWord = word.toLowerCase();

        for (int i = 0; i < lowerWord.length(); i++) {
            String current = String.valueOf(lowerWord.charAt(i));
            if (!letters.contains(current)) {
                return false;
            }
            letters.remove(current);
        }
        return true;
    }
}
//END
