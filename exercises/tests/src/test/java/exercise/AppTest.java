package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AppTest {
    List<Integer> listInt;

    @BeforeEach
    void initIntegerList() {
        this.listInt = new ArrayList<>();
        this.listInt.add(1);
        this.listInt.add(2);
        this.listInt.add(3);
        this.listInt.add(4);
        this.listInt.add(5);
        this.listInt.add(6);
    }

    @Test
    void testTake() {
        // BEGIN
        List<Integer> testList = Arrays.asList(1, 2, 3, 4);
        Assertions.assertEquals(App.take(this.listInt, 4), testList);

        List<Integer> testList2 = Arrays.asList(1, 2, 3, 4, 5, 6);
        Assertions.assertEquals(App.take(this.listInt, 9), testList2);

        List<Integer> testList3 = Arrays.asList();
        Assertions.assertEquals(App.take(this.listInt, 0), testList3);
        // END
    }
}
